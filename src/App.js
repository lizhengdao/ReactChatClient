import React, { Component } from 'react';
import Login from './components/LoginComponent'
import ChatBoxComponent from './components/chatboxComponent'
import {
  Route,
  Link,
  Switch
} from 'react-router-dom'
class App extends Component {
  render() {
    return (
      <div>
         
        <Login/>
        <Switch>
          <Route exact path="/" component={Login}/>
          <Route exact path="/chatbox" component={ChatBoxComponent}/>
        </Switch>
      </div>
       
    );
  }
}

export default App;
