import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.css';
import App from './App';
import Header from './components/headerComponent'
import registerServiceWorker from './registerServiceWorker'
import Routes from './routes/routes'

ReactDOM.render(
 <div>
<Header/>
<BrowserRouter>
  <Routes />
</BrowserRouter>
</div>

,
 document.getElementById('root'));
registerServiceWorker();
